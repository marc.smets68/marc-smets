import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clients: any = [];

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getClients();
  }

  getClients() {
    this.clients = [];
    this.rest.getClients().subscribe((data: {}) => {
      console.log(data);
      this.clients = data;
    });
  }

  add() {
    this.router.navigate(['/client-add']);
  }

  delete(id) {
    this.rest.deleteClient(id)
      .subscribe(res => {
        this.getClients();
      }, (err) => {
        console.log(err);
      }
      );
  }

}
