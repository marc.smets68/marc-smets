import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';



const endpoint = 'https://localhost:443258/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'aplication/json'
  })
};


@Injectable()

export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getClients(): Observable<any> {
    return this.http.get(endpoint + 'client').pipe(
      map(this.extractData));
  }

  getClient(id): Observable<any> {
    return this.http.get(endpoint + 'client' + id).pipe(
      map(this.extractData));
  }

  addClient(client): Observable<any> {
    console.log(client);
    return this.http.post<any>(endpoint + 'client', JSON.stringify(client), httpOptions).pipe(
      tap((client) => console.log(`added client w/ id=${client.id}`)),
      catchError(this.handleError<any>('addClient'))
    );
  }

  updateClient(id, client): Observable<any> {
    return this.http.put(endpoint + 'client' + id, JSON.stringify(client), httpOptions).pipe(
      tap(_ => console.log(`updated client id=${id}`)),
      catchError(this.handleError<any>('updateClient'))
    );
  }

  deleteClient(id): Observable<any> {
    return this.http.delete<any>(endpoint + 'client/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted client id=${id}`)),
      catchError(this.handleError<any>('deleteClient'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}



