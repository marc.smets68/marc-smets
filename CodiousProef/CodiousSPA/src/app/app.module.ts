import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/Forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientComponent } from './client/client.component';

import { ConsultantComponent } from './consultant/consultant.component';
import { ProjectComponent } from './project/project.component';
import { AppointmentComponent } from './appointment/appointment.component';

const appRoutes: Routes = [
  {
    path: 'client',
    component: ClientComponent,
    data: { title: 'Client List' }
  },
  
  {
    path: '',
    redirectTo: 'client',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ClientComponent,
    
    ConsultantComponent,
    ProjectComponent,
    AppointmentComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
