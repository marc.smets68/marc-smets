import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from './Client';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CodiousSPA';
  readonly ROOT_URL = 'http://localhost:51909';

  clients: Observable<Client[]>;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    let obs = this.http.get('https://localhost:44358/api/');
    obs.subscribe((response) => console.log(response));
  }
  


  
}
