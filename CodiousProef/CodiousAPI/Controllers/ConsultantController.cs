﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CodiousProef.Repositories;
using CodiousProef.Data;

namespace CodiousAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultantController : ControllerBase
    {
        private readonly IConsultantRepository _consultantRepository;

        public ConsultantController(IConsultantRepository consultantRepository)
        {
            _consultantRepository = consultantRepository;
        }

        // GET: api/Consultant
        [HttpGet]
        public IEnumerable<Consultant> Get()
        {
            return _consultantRepository.GetAllConsultants();
            //return new string[] { "value1", "value2" };
        }

        // GET: api/Consultant/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Consultant
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Consultant/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
