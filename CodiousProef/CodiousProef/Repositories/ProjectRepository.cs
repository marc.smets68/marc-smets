﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public ProjectRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IEnumerable<Project> Projects { get { return _dbContext.Projects; } }

        public void CreateProject(Project project)
        {
            _dbContext.Projects.Add(project);
            _dbContext.SaveChanges();
        }

        public void DeleteProject(int id)
        {
            var project = _dbContext.Projects.Find(id);
            _dbContext.Projects.Remove(project);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return _dbContext.Projects;
        }

        public Project GetProject(int id)
        {
            return _dbContext.Projects.Find(id);
            
        }

        public void UpdateProject(Project project)
        {
            _dbContext.Projects.Update(project);
            _dbContext.SaveChanges();
        }
    }
}
