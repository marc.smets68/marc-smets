﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private ApplicationDbContext _dbContext;

        public ClientRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Client> Clients { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void CreateClient(Client client)
        {
            _dbContext.Clients.Add(client);
            _dbContext.SaveChanges();
        }

        public void DeleteClient(int id)
        {
            var client = _dbContext.Clients.Find(id);
            _dbContext.Clients.Remove(client);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Client> GetAllClients()
        {
            return _dbContext.Clients.ToList();
        }

        public Client GetClient(int id)
        {
            return _dbContext.Clients.Find(id);
        }

        public void UpdateClient(Client client)
        {
            _dbContext.Update(client);
            _dbContext.SaveChanges();
        }
    }
}
