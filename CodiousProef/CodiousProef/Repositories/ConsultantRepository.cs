﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public class ConsultantRepository : IConsultantRepository
    {
        private ApplicationDbContext _dbContext;

        public ConsultantRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Consultant> Consultants { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void CreateConsultant(Consultant consultant)
        {
            _dbContext.Consultants.Add(consultant);
            _dbContext.SaveChanges();
        }

        public void DeleteConsultant(int id)
        {
            var consultant = _dbContext.Consultants.Find(id);
            _dbContext.Consultants.Remove(consultant);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Consultant> GetAllConsultants()
        {
            return _dbContext.Consultants.ToList();
        }

        public Consultant GetConsultant(int id)
        {
            return _dbContext.Consultants.Find(id);
        }

        public void UpdateConsultant(Consultant consultant)
        {
            _dbContext.Update(consultant);
            _dbContext.SaveChanges();
        }
    }
}
