﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public interface IConsultantRepository
    {
        IEnumerable<Consultant> Consultants { get; set; }

        IEnumerable<Consultant> GetAllConsultants();
        Consultant GetConsultant(int id);
        void CreateConsultant(Consultant consultant);
        void DeleteConsultant(int id);
        void UpdateConsultant(Consultant consultant);
    }
}
