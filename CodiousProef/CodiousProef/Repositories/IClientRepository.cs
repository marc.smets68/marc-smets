﻿using CodiousProef.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Repositories
{
    public interface IClientRepository
    {
        IEnumerable<Client> Clients { get ; set; }

        IEnumerable<Client> GetAllClients();
        Client GetClient(int id);
        void CreateClient(Client client);
        void DeleteClient(int id);
        void UpdateClient(Client client);
    }
}
