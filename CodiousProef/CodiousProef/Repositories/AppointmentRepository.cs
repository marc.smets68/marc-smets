﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public class AppointmentRepository : IAppointmentRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public AppointmentRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IEnumerable<Appointment> Appointments => _dbContext.Appointments;

        public void CreateAppointment(Appointment appointment)
        {
            _dbContext.Appointments.Add(appointment);
            _dbContext.SaveChanges();
        }

        public void DeleteAppointment(int id)
        {
            var appointment = _dbContext.Appointments.Find(id);
            _dbContext.Appointments.Remove(appointment);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Appointment> GetAllAppointments()
        {
            return _dbContext.Appointments;
        }

        public Appointment GetAppointment(int id)
        {
            return  _dbContext.Appointments.Find(id);
            
        }

        public void UpdateProject(Appointment appointment)
        {
            _dbContext.Appointments.Update(appointment);
            _dbContext.SaveChanges();
        }
    }
}
