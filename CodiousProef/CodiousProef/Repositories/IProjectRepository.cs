﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public interface IProjectRepository
    {
        IEnumerable<Project> Projects { get;  }

        IEnumerable<Project> GetAllProjects();
        Project GetProject(int id);
        void CreateProject(Project project);
        void DeleteProject(int id);
        void UpdateProject(Project project);
    }
}
