﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodiousProef.Data;

namespace CodiousProef.Repositories
{
    public interface IAppointmentRepository
    {
        IEnumerable<Appointment> Appointments { get; }

        IEnumerable<Appointment> GetAllAppointments();
        Appointment GetAppointment(int id);
        void CreateAppointment(Appointment appointment);
        void DeleteAppointment(int id);
        void UpdateProject(Appointment appointment);
    }
}
