#pragma checksum "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "518fa16388e95e7a0cecbde33617d98c59236dea"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Consultant_Details), @"mvc.1.0.view", @"/Views/Consultant/Details.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Consultant/Details.cshtml", typeof(AspNetCore.Views_Consultant_Details))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\_ViewImports.cshtml"
using CodiousProef;

#line default
#line hidden
#line 2 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\_ViewImports.cshtml"
using CodiousProef.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"518fa16388e95e7a0cecbde33617d98c59236dea", @"/Views/Consultant/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c1b85104be4f1c93d3043f641b40e29c70af77e0", @"/Views/_ViewImports.cshtml")]
    public class Views_Consultant_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CodiousProef.Data.Consultant>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(37, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
  
    ViewData["Title"] = "Details";

#line default
#line hidden
            BeginContext(82, 131, true);
            WriteLiteral("\r\n<h1>Details</h1>\r\n\r\n<div>\r\n    <h4>Consultant</h4>\r\n    <hr />\r\n    <dl class=\"row\">\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(214, 40, false);
#line 14 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Name));

#line default
#line hidden
            EndContext();
            BeginContext(254, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(316, 36, false);
#line 17 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Name));

#line default
#line hidden
            EndContext();
            BeginContext(352, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(413, 45, false);
#line 20 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Uurtarief));

#line default
#line hidden
            EndContext();
            BeginContext(458, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(520, 41, false);
#line 23 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Uurtarief));

#line default
#line hidden
            EndContext();
            BeginContext(561, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(622, 41, false);
#line 26 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(663, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(725, 37, false);
#line 29 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(762, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(823, 48, false);
#line 32 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Adres.Street));

#line default
#line hidden
            EndContext();
            BeginContext(871, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(933, 44, false);
#line 35 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Adres.Street));

#line default
#line hidden
            EndContext();
            BeginContext(977, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1038, 48, false);
#line 38 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Adres.Number));

#line default
#line hidden
            EndContext();
            BeginContext(1086, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1148, 44, false);
#line 41 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Adres.Number));

#line default
#line hidden
            EndContext();
            BeginContext(1192, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1253, 49, false);
#line 44 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Adres.ZipCode));

#line default
#line hidden
            EndContext();
            BeginContext(1302, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1364, 45, false);
#line 47 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Adres.ZipCode));

#line default
#line hidden
            EndContext();
            BeginContext(1409, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1470, 47, false);
#line 50 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Adres.Place));

#line default
#line hidden
            EndContext();
            BeginContext(1517, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1579, 43, false);
#line 53 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
       Write(Html.DisplayFor(model => model.Adres.Place));

#line default
#line hidden
            EndContext();
            BeginContext(1622, 47, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1669, 54, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "518fa16388e95e7a0cecbde33617d98c59236dea10457", async() => {
                BeginContext(1715, 4, true);
                WriteLiteral("Edit");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 58 "C:\Users\marcs\source\repos\Marc Smets\CodiousProef\CodiousProef\Views\Consultant\Details.cshtml"
                           WriteLiteral(Model.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1723, 8, true);
            WriteLiteral(" |\r\n    ");
            EndContext();
            BeginContext(1731, 38, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "518fa16388e95e7a0cecbde33617d98c59236dea12796", async() => {
                BeginContext(1753, 12, true);
                WriteLiteral("Back to List");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1769, 10, true);
            WriteLiteral("\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CodiousProef.Data.Consultant> Html { get; private set; }
    }
}
#pragma warning restore 1591
