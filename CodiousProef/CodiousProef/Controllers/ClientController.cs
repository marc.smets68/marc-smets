﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using CodiousProef.Data;
using CodiousProef.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodiousProef.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientRepository _clientRepository;
        public ClientController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }
        // GET: api/Client
        [HttpGet]
        public IEnumerable<Client> Get()
        {
            return _clientRepository.GetAllClients();
        }

        // GET: api/Client/5
        [HttpGet("{id}", Name = "Get")]
        public Client Get(int id)
        {
            return _clientRepository.GetClient(id);
        }

        // POST: api/Client
        [HttpPost]
        public void Post(Client client)
        {
            _clientRepository.CreateClient(client);
            

        }

        // PUT: api/Client/5
        [HttpPut("{id}")]
        public void Put(int id, Client client)
        {
            if(id == client.Id)
            {
                _clientRepository.UpdateClient(client);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _clientRepository.DeleteClient(id);
        }
    }
}
