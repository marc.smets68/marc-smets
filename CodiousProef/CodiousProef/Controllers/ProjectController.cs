﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CodiousProef.Repositories;
using CodiousProef.Data;

namespace CodiousProef.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectRepository _projectRepository;
        public ProjectController(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }
        // GET: api/Project
        [HttpGet]
        public IEnumerable<Project> Get()
        {
            return _projectRepository.GetAllProjects();
        }

        // GET: api/Project/5
        [HttpGet("{id}", Name = "GetProject")]
        public Project Get(int id)
        {
            return _projectRepository.GetProject(id);
        }

        // POST: api/Project
        [HttpPost]
        public void Post(Project project)
        {
            
            _projectRepository.CreateProject(project);
        }

        // PUT: api/Project/5
        [HttpPut("{id}")]
        public void Put(int id, Project project)
        {
            if (id == project.Id)
            {
                _projectRepository.UpdateProject(project);
            }

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectRepository.DeleteProject(id);
        }
    }
}
