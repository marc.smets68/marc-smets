﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CodiousProef.Repositories;
using CodiousProef.Data;

namespace CodiousProef.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultantController : ControllerBase
    {
        private readonly IConsultantRepository _consultantRepository;
        public ConsultantController(IConsultantRepository consultantRepository)
        {
            _consultantRepository = consultantRepository;
        }

        // GET: api/Consultant
        [HttpGet]
        public IEnumerable<Consultant> Get()
        {
           return _consultantRepository.GetAllConsultants();
        }

        // GET: api/Consultant/5
        [HttpGet("{id}", Name = "GetConsultant")]
        public Consultant Get(int id)
        {
            return _consultantRepository.GetConsultant(id);
        }

        // POST: api/Consultant
        [HttpPost]
        public void Post(Consultant consultant)
        {
            _consultantRepository.CreateConsultant(consultant);
        }

        // PUT: api/Consultant/5
        [HttpPut("{id}")]
        public void Put(int id, Consultant consultant)
        {
            if(id == consultant.Id)
            {
                _consultantRepository.UpdateConsultant(consultant);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _consultantRepository.DeleteConsultant(id);
        }
    }
}
