﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Data
{
    public class Client
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Function { get; set; }
        public List<Contact> Contacts { get; set; }
        public Adres Adres { get; set; }

        
        public string Email { get; set; }
    }
}
