﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Data
{
    public class Project
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public List<Consultant> Consultants { get; set; }
        public string Description { get; set; }
        public TimeSpan Duration { get; set; }
        
    }
}
