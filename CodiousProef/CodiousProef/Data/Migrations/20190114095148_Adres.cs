﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodiousProef.Data.Migrations
{
    public partial class Adres : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AdresId",
                table: "Consultants",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AdresId",
                table: "Clients",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Consultants_AdresId",
                table: "Consultants",
                column: "AdresId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_AdresId",
                table: "Clients",
                column: "AdresId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Adres_AdresId",
                table: "Clients",
                column: "AdresId",
                principalTable: "Adres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Consultants_Adres_AdresId",
                table: "Consultants",
                column: "AdresId",
                principalTable: "Adres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Adres_AdresId",
                table: "Clients");

            migrationBuilder.DropForeignKey(
                name: "FK_Consultants_Adres_AdresId",
                table: "Consultants");

            migrationBuilder.DropIndex(
                name: "IX_Consultants_AdresId",
                table: "Consultants");

            migrationBuilder.DropIndex(
                name: "IX_Clients_AdresId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "AdresId",
                table: "Consultants");

            migrationBuilder.DropColumn(
                name: "AdresId",
                table: "Clients");
        }
    }
}
