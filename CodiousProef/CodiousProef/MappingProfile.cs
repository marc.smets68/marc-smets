﻿using AutoMapper;
using CodiousProef.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AppUser, RegistrationViewModel>().ReverseMap();
        }
    }
}
