﻿using CodiousProef.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Services
{
    public interface IUserService
    {
        AppUser Authenticate(string username, string password);
        IEnumerable<AppUser> GetAll();
        AppUser GetById(int id);
        AppUser Create(AppUser user, string password);
        void Update(AppUser user, string password = null);
        void Delete(int id);
    }
}
